import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
user_type;
booksList:any = [];
  constructor(
    private _apiService:ApiService,
    private toastr: ToastrService,
    private router:Router
  ) { 
    // refresh the book list always so that it has latest data
    // do not reuse the previous book list
    this.router.routeReuseStrategy.shouldReuseRoute = 
    function() {
      return false;
    }
  }

  // we have to get user, token 
  ngOnInit() {
    let token = localStorage.getItem('token');
    this.user_type = localStorage.getItem('user_type');
    // if not token, then route back to login page
    if(!token) {
      this.router.navigate(['/login']);
    }
    // get all the books
    this._apiService.getBooks().subscribe(
      data => {
        this.booksList = data['books'];
      },
      error => {
        this.toastr.error(error.error.message, "Error");
        this.router.navigate(['/login']);
      }
    )
  }
  
  deleteBook(id:any) {
    if(this.user_type == 'admin') {
      this._apiService.delete_book(id).subscribe(
        data => {
          console.log(data);
          this.router.navigated = false; // to refresh and not get any stale data
          this.router.navigate(['/books']);
        },
        error => {
          console.log(error);
        }
      )
    } else {
      this.toastr.error("You are not an Admin.", "Error");
    }
  }

  logout(id:any) {
    localStorage.removeItem('token');
    localStorage.removeItem('user_type');
    this.router.navigate(['/login']);
  }
}
