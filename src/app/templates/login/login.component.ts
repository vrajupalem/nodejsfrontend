import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public model:any ={}
  response:any;
  constructor(
    private _apiService:ApiService,
    private toastr: ToastrService,
    private router:Router
  ) { }

  ngOnInit() {
  }

  signin () {
    console.log("login.component.ts: signin() called")
    this._apiService.login(this.model).subscribe(
      data => {
        console.log("data in signin: ", data);
        this.response = data;
        // check the response message
        console.log(this.response.message)
        if (this.response.message === "Auth successful") {
          console.log("login.component.ts data: ", data); 
          // console.log("data: ", data);
          this.toastr.success(data["message"], "Success");
          localStorage.setItem('user_type', data['user_type']);
          localStorage.setItem('token', data['token']);
          console.log("navigating to all-books page")
          this.router.navigate(['./all-books']);
        }
      },
      error => {
        this.toastr.error(error.error.message, "Error in login.component.ts");
        //this.toastr.error(error.error.message, "Error");
      }
      
    )
  }
}
