import { Injectable } from '@angular/core';
// importing libraries for making HTTP calls
import {HttpClient, HttpHeaders, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs"; // from React

@Injectable()
export class ApiService { 

  constructor(private httpClient: HttpClient) { }
     // for login
     login(model:any) {
         console.log("api.service.ts: constructor login");
        // return this.httpClient.post('http://127.0.0.1:3000/user/login', model);
         return this.httpClient.post('http://localhost:4000/user/login', model);
     }

     signup(model:any) {
        console.log("api.service.ts: constructor signup");
        //return this.httpClient.post('http://127.0.0.1:3000/user/signup', model);
        return this.httpClient.post('http://localhost:4000/user/signup', model);
     }
     addNewBook(model:any) {
        console.log("api.service.ts: constructor addNewBook");
        return this.httpClient.post('http://localhost:4000/books/add-book', model);
     }
     getBooks() {
           console.log("api.service.ts: constructor getBooks");
          return this.httpClient.get('http://localhost:4000/books/all-books');
     }
     delete_book(id:any) {
          return this.httpClient.delete('http://localhost:4000/books/delete-book/'+id);
     }

    bookDetail(id:any) {
      return this.httpClient.get('http://localhost:4000/books/book-details/'+id);
    }

    editBook(id:any, model:any) {
      console.log("api.service.ts: constructor editBook");
      console.log(id)
      console.log(model)
      return this.httpClient.patch('http://localhost:4000/books/update-book/'+id, model);
    }
}
  

  

